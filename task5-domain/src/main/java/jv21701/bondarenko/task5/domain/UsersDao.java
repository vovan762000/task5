
package jv21701.bondarenko.task5.domain;

public interface UsersDao extends DAO<Users, Integer> {

    Users getByUsername(String username);
    
}
