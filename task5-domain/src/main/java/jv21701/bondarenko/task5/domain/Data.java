
package jv21701.bondarenko.task5.domain;

import java.util.List;

public class Data {

    public Data() {
    }
        
    private List<Users> userses = new UsersDaoImpl().getList();
    
    public List<Users> getUserses() {
        return userses;
    }

    public void setUserses(List<Users> users) {
        this.userses = users;
    }
}
