
package jv21701.bondarenko.task5.domain;

import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

public class UsersDaoImpl extends AbstractDao<Users, Integer> implements UsersDao {

    @Override
    public Users getByUsername(String login) {
        Criteria criteria = getCurrentSession().createCriteria(Users.class)
                .add(Restrictions.eq("login", login));
        List<Users> users = criteria.list();
        return (users.size() > 0) ? users.get(0) : null;
    }

}
