
package jv21701.bondarenko.task5.domain;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.Transaction;

public abstract class AbstractDao<T, I extends Serializable> implements DAO<T, I> {

    private final Class<T> entityClass;

    protected Session getCurrentSession() {
        return HibernateUtil.getSessionFactory().openSession();
    }

    public AbstractDao() {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass()
                .getGenericSuperclass();
        this.entityClass = (Class<T>) genericSuperclass
                .getActualTypeArguments()[0];
    }

    @Override
    public T getByID(I key) {
        Session session = getCurrentSession();
        return (T) session.load(entityClass, key);
    }

    @Override
    public T create(T object) {
        Session session = getCurrentSession();
        try {
            session.getTransaction().begin();
            return (T) session.save(object);
        } finally {
            session.getTransaction().commit();
        }

    }

    @Override
    public boolean update(T object) {
        Session session = getCurrentSession();
        Transaction trank = session.getTransaction();
        try {
            session.setFlushMode(FlushMode.AUTO);
            trank.begin();
            session.merge(object);
            trank.commit();
            return true;
        } catch(Exception e){
            trank.rollback();
            return false;
        }
    }

    @Override
    public boolean remove(T object) {
        Session session = getCurrentSession();
        session.delete(object);
        return true;
    }

    @Override
    public List<T> getList() {
        Session session = getCurrentSession();
        return (List<T>) session.createCriteria(entityClass).list();
    }

}
