
package jv21701.bondarenko.task5.domain;

import java.util.List;

public interface DAO<T,I> {
    T getByID(I key);

    T create(T o);

    boolean update(T o);

    boolean remove(T o);

    List<T> getList();
}
