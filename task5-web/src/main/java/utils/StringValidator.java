package utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringValidator {
    
    private static final String EMAIL_PATTERN = "^([a-z0-9_-]+\\.)*[a-z0-9_-]+@[a-z0-9_-]+(\\.[a-z0-9_-]+)*\\.[a-z]{2,6}$";
private static final String PASSWORD_PATTERN = "^[a-zA-Z0-9]{3,12}$";
    public static boolean isEmpty(String data) {
        if (data == null) {
            return true;
        }
//        data = data.trim();
//        if (data.length() == 0) {
//            return true;
//        }
//        return false;
        return data.isEmpty();
    }
    
    public  static  boolean isEmailValid(String email){
    Pattern pattern = Pattern.compile(EMAIL_PATTERN);
    Matcher matcher = pattern.matcher(email);
    return matcher.find();
    }
    
    public  static  boolean isPasswordValid(String password){
    Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
    Matcher matcher = pattern.matcher(password);
    return matcher.find();
    }
    
    private static boolean findMatches(String pattern,String value){
    return false;
    }
}
