package jv21701.bondarenko.task5.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import jv21701.bondarenko.task5.domain.Users;
import jv21701.bondarenko.task5.domain.Users.Role;
import jv21701.bondarenko.task5.domain.UsersDaoImpl;

@WebServlet(name = "RegisterServlet", urlPatterns = {"/register"})
public class RegisterServlet extends HttpServlet {

    UsersDaoImpl users = new UsersDaoImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/WEB-INF/views/register.jsp").forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String firsname = req.getParameter("firstname");
        String lastname = req.getParameter("lastname");
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        Users user = new Users(login, password, firsname, lastname, Role.USER);
        users.create(user);
        HttpSession session = req.getSession(true);
        session.setAttribute("user", user);
        resp.addCookie(new Cookie("sid", session.getId()));
        resp.sendRedirect("/task5-web/home");
    }

}
