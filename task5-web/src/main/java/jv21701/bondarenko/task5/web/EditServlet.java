package jv21701.bondarenko.task5.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import jv21701.bondarenko.task5.domain.Users;
import jv21701.bondarenko.task5.domain.UsersDaoImpl;

@WebServlet(name = "EditServlet", urlPatterns = {"/edit"})
public class EditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int userId;
        try {
            userId = Integer.parseInt(req.getParameter("id"));
        } catch (NumberFormatException e) {
            resp.sendError(404, "Such user not found");
            return;
        }
        Users user = new UsersDaoImpl().getByID(userId);
        if (user != null) {
            HttpSession session = req.getSession();
            Users user_ = (Users) session.getAttribute("user");
            req.setAttribute("firstname", user_.getFirstname());
            req.setAttribute("lastname", user_.getLastname());
            req.setAttribute("user", user);
            req.getRequestDispatcher("/WEB-INF/views/home/edit.jsp").forward(req, resp);
        } else {
            resp.sendError(404, "Such user not found");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int userId;
        try {
            userId = Integer.parseInt(req.getParameter("id"));
        } catch (NumberFormatException e) {
            resp.sendError(404, "Such user not found");
            return;
        }
        Users user = new UsersDaoImpl().getByID(userId);
        if (user != null) {
            String firstname = req.getParameter("firstname");
            String lastname = req.getParameter("lastname");
            if (firstname == null || firstname.isEmpty()) {
                //send error
                resp.sendError(500, "Empty firstname");
            }
            if (lastname == null || lastname.isEmpty()) {
                //send error
                resp.sendError(500, "Empty lastname");
            }
            user.setFirstname(firstname);
            user.setLastname(lastname);
            new UsersDaoImpl().update(user);
            resp.sendRedirect("/task5-web/home");
        } else {
            resp.sendError(404, "Such user not found");
        }
    }

    
}
