
package jv21701.bondarenko.task5.web;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import jv21701.bondarenko.task5.domain.Users;
import jv21701.bondarenko.task5.domain.UsersDaoImpl;


@WebServlet(name = "HomeServlet", urlPatterns = {"/home"})
public class HomeServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session =  req.getSession();
        Users user = (Users)session.getAttribute("user");
        req.setAttribute("firstname", user.getFirstname());
        req.setAttribute("lastname", user.getLastname());
        
        List<Users> users = new UsersDaoImpl().getList();
        req.setAttribute("users", users);
        req.getRequestDispatcher("/WEB-INF/views/home/home.jsp").forward(req, resp);
    }
}
