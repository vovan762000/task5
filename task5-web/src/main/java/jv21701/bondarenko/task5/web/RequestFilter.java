package jv21701.bondarenko.task5.web;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter(filterName = "RequestFilter", urlPatterns = {"/*"})
public class RequestFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
//        HttpSession session = ((HttpServletRequest) request).getSession();
//        Cookie sid = getCookie(request, session);
//        if (sid == null) {
//            ((HttpServletResponse) response).sendRedirect("index.jsp");
//
//        } else {
//            if (sid.getValue().isEmpty()) {
//                ((HttpServletResponse) response).sendRedirect("index.jsp");
//            }
//        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }

    private Cookie getCookie(ServletRequest request, HttpSession session) {
        Cookie[] cookies = ((HttpServletRequest) request).getCookies();
        for (Cookie cooky : cookies) {
            if (cooky.getName().equals("sid")) {
                return cooky;
            }
        }
        return null;
    }

}
